<?php
/*
 * This file is part of CLAIRE.
 *
 * CLAIRE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLAIRE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLAIRE. If not, see <http://www.gnu.org/licenses/>
 */

namespace SimpleIT\ClaireExerciseBundle\Service\Directory;

use SimpleIT\ClaireExerciseBundle\Controller\Frontend\AdminController;
use SimpleIT\ClaireExerciseBundle\Entity\StatView;
use SimpleIT\ClaireExerciseBundle\Entity\AskerUser;
use SimpleIT\ClaireExerciseBundle\Service\TransactionalService;
use SimpleIT\ClaireExerciseBundle\Entity\Directory;
use SimpleIT\ClaireExerciseBundle\Entity\AskerUserDirectory;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Service which manages the stored exercises
 *
 * @author Baptiste Cablé <baptiste.cable@liris.cnrs.fr>
 */
#class DirectoryService extends TransactionalService implements AnswerServiceInterface
class DirectoryService extends TransactionalService
{
    /**
     * @var exerciseModelService
     */
    private $exerciseModelService;
    /**
     * @var askerUserDirectoryService
     */
    private $askerUserDirectoryService;
    /**
     * @var DirectoryRepository
     */
    private $directoryRepository;
    /**
     * Set directoryRepository
     *
     * @param UserRepository $directoryRepository
     */
    public function setDirectoryRepository($directoryRepository)
    {
        $this->directoryRepository = $directoryRepository;
    }

    /**
     * Set askerUserDirectoryService
     *
     * @param ExerciseModel $exerciseModelService
     */
    public function setExerciseModelService($exerciseModelService)
    {
        $this->exerciseModelService = $exerciseModelService;
    }


    /**
     * Set exerciseModelService
     *
     * @param UserRepository $askerUserDirectoryService
     */
    public function setaskerUserDirectoryService($askerUserDirectoryService)
    {
        $this->askerUserDirectoryService = $askerUserDirectoryService;
    }


    public function all(){
        return $this->directoryRepository->findAll();
    }

    public function countCurrentStudents($dir,$teachers)
    {
        return $this->directoryRepository->countCurrentStudents($dir, $teachers);
    }

    public function countOldStudents($dir,$teachers)
    {
        return $this->directoryRepository->countOldStudents($dir, $teachers);
    }

    public function nativeParents(){
        return $this->directoryRepository->findNativeParents();

    }
    public function allParents($user = 0)
    {
        return $this->directoryRepository->findParents($user);
    }
	// dead code RC 24/07/2023
    //public function findMine(AskerUser $user)
    //{
    //    return $this->directoryRepository->findMine($user->getId());
    //}

    public function findAllModelsIds($id)
    {
        return $this->directoryRepository->findAllModelsIds($id);
    }

    public function find($id)
    {
        return $this->directoryRepository->find($id);
    }
    public function findOneByName($name)
    {
        return $this->directoryRepository->findOneByName($name);
    }
    public function remove(Directory $id, AskerUser $user)
    {
        if (
            $id->getOwner()->getId() == $user->getId()
            || $user->isAdmin()
        ){
            try{
                $entity = $this->directoryRepository->find($id->getId());
                foreach($entity->getModels() as $mod){
                    $entity->removeModel($mod);
                }
                $this->em->remove($entity);
                $this->em->flush();
            }catch (ForeignKeyConstraintViolationException $e){
                $res= new Response('Il est nécessaire de supprimer les sous-dossiers:'. $e->getMessage(), 500);
                $res->send();
            }
        }else{
            throw new AccessDeniedException();
        }
    }

    public function activateComper($resource){
        $entity = $this->find($resource->getId());
        $userCreated = 0;
        if (!$entity->getParent()){
            // Create comper profile if they doesn't exist
            if($entity->getFrameworkId() !== null){

                $adminController = new AdminController();
				/* createGroup est un appel au service depuis le controller
				 * => un peu la flemme mettre la suite au propre addComperTouser devrait tre dans un autre service
				*/
                //$adminController->createGroup($resource->getFrameworkId(), $resource->getId(), $resource->getName());

                $userIds = $this->getIdUsers($entity, null);

                foreach($userIds as $userId){
                    $profileCreated = $adminController->addComperToUser($resource->getFrameworkId(), $userId, 'learner', $resource->getId() );
                    if ($profileCreated){
                        $userCreated = $userCreated + 1;
                    }
                }
            }
        }
        return $userCreated;
    }
	/*
	 * remove RC 26/07/2023

    //public function createGroup($resource){
        $entity = $this->find($resource->getId());
        $groupCreated = false;
        if (!$entity->getParent()) {
            // Create comper profile if they doesn't exist
            if ($entity->getFrameworkId() !== null) {

                $adminController = new AdminController();
                return $adminController->createGroup($resource->getFrameworkId(), $resource->getId(), $resource->getName());
            }
        }
        return false;
    }
	*/

    public function listUsers($resource){
        $entity = $this->find($resource->getId());
        $view = $resource->getLastView();
        if (!$entity->getParent()){
            if($entity->getFrameworkId() !== null){
                return $this->getIdUsers($entity, $view);
            }
        }
        return [];
    }

    public function listManagers($resource){
        $entity = $this->find($resource->getId());
        if (!$entity->getParent()){
            if($entity->getFrameworkId() !== null){
                return $entity->getManagers();
            }
        }
        return [];
    }

    public function activateComperUser($resource, $userId, $role = "learner"){
        $entity = $this->find($resource->getId());
        $profileCreated = false;
        if (!$entity->getParent()){
            // Create comper profile if they doesn't exist
            if($entity->getFrameworkId() !== null){
                $adminController = new AdminController();
                $profileCreated = $adminController->addComperToUser($resource->getFrameworkId(), $userId, $role, $resource->getId());
            }
        }
        return $profileCreated;
    }

    public function activateComperTeacher($resource, $userId, $role){
        $entity = $this->find($resource->getId());
        $rightsManaged = false;
        if (!$entity->getParent()){
            if($entity->getFrameworkId() !== null){
                $adminController = new AdminController();
                $rightsManaged = $adminController->addRoleToTeacher($resource->getFrameworkId(), $userId, $role, $resource->getId());
            }
        }
        return $rightsManaged;
    }



    public function edit($resource,AskerUser $user)
    {
        $userId = $user->getId();
        $find = 0;
        if (is_null($resource->getId())) {
            throw new MissingIdException();
        }
        $entity = $this->find($resource->getId());
        if ($entity->getOwner()->getId() !== $userId
            && !$entity->hasManager($user)
        ){
            throw new AccessDeniedException();
        }
        $entity->setIsVisible($resource->getIsVisible());
        $entity->setName($resource->getName());
		//  the directory is root level
        if (!$entity->getParent()){
            // Check difference between frameworkId
            $frameworkChanged = ($resource->getFrameworkId() !== $entity->getFrameworkId());

            $entity->setCode($resource->getCode());
            $entity->setFrameworkId($resource->getFrameworkId());
            $this->askerUserDirectoryService->updateManager($entity,$resource);
            $this->askerUserDirectoryService->updateReader($entity,$resource);
            foreach($entity->getSubs() as $dir){
                $this->askerUserDirectoryService->updateManager($dir, $resource);
                $this->askerUserDirectoryService->updateReader($dir, $resource);
            }
            // Create comper profile if they doesn't exist
            /*
            if($resource->getFrameworkId() !== null && $frameworkChanged){
                $userIds = $this->getIdUsers($entity, null);
                $adminController = new AdminController();
                foreach($userIds as $userId){
                    $profileCreated = $adminController->addComperToUser($resource->getFrameworkId(), $userId);
                }
            }
            */
            // Update sub-dirs with frameworkId
            if($resource->getFrameworkId() !== null && $frameworkChanged){
                foreach($entity->getSubs() as $dir ){
                    $this->updateFrameworkDir($dir, $resource->getFrameworkId());
                }
            }

		}else {
            foreach($entity->getParent()->getSubs() as $dir){
                $this->askerUserDirectoryService->updateManager($dir, $resource);
                $this->askerUserDirectoryService->updateReader($dir, $resource);
            }
		}
        foreach($entity->getModels() as $model){
            $entity->removeModel($model);
        }
        if (!is_null($resource->getModels())){
            $repo = $this->em
                ->getRepository('SimpleITClaireExerciseBundle:ExerciseModel\ExerciseModel')
            ;
            foreach($resource->getModels() as $model){
                $entity->addModel($repo->find($model->getId()));
            }
        }
        $this->em->flush();

        if (!is_null($resource->getModels())){
            for($i=0; $i<count($resource->getModels()); $i++){
                $this->directoryRepository->updateVisibleExercise(
                    $resource->getVisibleExercise()[$i],
                    $resource->getId(),
                    $resource->getModels()[$i]->getId()
                );
            }
        }
        $entity->setVisibleExercise($resource->getVisibleExercise());
        return $entity;
    }
    public function updateFrameworkDir($dir, $frameworkId)
    {
        $entity = $this->find($dir->getId());
        $entity->setFrameworkId($frameworkId);
        $this->em->persist($entity);
        //$this->em->flush($entity);
    }


	public function generateModelsToClone(&$models, $user, $directory)
	{
		foreach($directory->getModels() as $m){
			if (!isset($models[$m->getId()])){
				$models[$m->getId()] = $this->exerciseModelService->import(
					$user->getId(),
					$m
				);
			}
		}
	}
	public function getRequiredModels($user, $directory)
	{
		$models = array();
		$this->generateModelsToClone($models, $user, $directory);
		foreach($directory->getSubs() as $sub){
			$this->generateModelsToClone($models, $user, $sub);
		}
		return $models;
	}
    /*
     * $user => Askeruser
     * $directory => Directory
     * $isOwner if owner => duplicate else import
     */
	public function duplicate($user, $directory, $isOwner)
	{
        $action = "Duplicat - ";
        if (!$isOwner){
            //$action = "Import - ";
		    $this->exerciseModelService->setForcedImport(true);
		    $models = $this->getRequiredModels($user, $directory);
        }
		$newDirectory = $this->create($user, 0);
		$newDirectory->setName($action .$directory->getName());
		$newDirectory->setFrameworkId($directory->getFrameworkId());
        $newDirectory->setIsVisible($directory->getIsVisible());
		foreach($directory->getModels() as $m){
			$newDirectory->addModel($m);
		}
		foreach($directory->getSubs() as $sub){
			$subDirectory = $this->create($user, 0);
			$subDirectory->setParent($newDirectory);
            $subDirectory->setIsVisible($sub->getIsVisible());
			$subDirectory->setName($sub->getName());
			foreach($sub->getModels() as $m){
				$subDirectory->addModel($m);
			}
		}
        $this->em->flush();
		$this->exerciseModelService->setForcedImport(0);
        return $newDirectory;
	}

    public function create($user, $directory)
    {
        $dir = new Directory();
        $dir->setName('');
        $dir->setIsVisible(true);
        $dir->setOwner($user);
        $dirUser = new AskerUserDirectory();
        $dirUser->setUser($user);
        $dirUser->setIsManager(false);
        $dirUser->setIsReader(false);
        $dirUser->setDirectory($dir);
        $this->em->persist($dirUser);
        $this->em->persist($dir);
        if ($directory != 0){
			$parent = $this->directoryRepository->find($directory);
            if ($parent->getFrameworkId() !== null) {
                $dir->setFrameworkId($parent->getFrameworkId());
            }

            $dir->setParent($parent);
            $this->askerUserDirectoryService->updateManager($dir, $parent);
            $this->askerUserDirectoryService->updateReader($dir,$parent);
        }
        $this->em->flush();
        return $dir;
    }
    public function JSONstats($repo,Directory $directory, $model, $view, $ids)
    {
        $datas = $repo->
        distributionMarkByModel($model->getId(),$view, $ids)[0]
        ;
        $json = array();
        foreach($datas as $key=> $val){
            $json[] = array('range' => $key, 'nb' => (int)$val);
        }
        return $json;
    }
    // Permet de chercher les informations pour le diagrame sunburst
    public function JSONUserStats($directory,$user,$view)
    {
        $models = $this->directoryRepository->
        findAllModels($directory->getId())
        ;

        $json = array();
        foreach ($models as $key => $model) {
            $json[$key] = $this->directoryRepository->
            JSONUserStats($model['mid'],$model['did'], $user->getId(), $view)[0]
            ;
        }
        foreach ($json as $key => $dir) {
            if($dir['totalAtt'] == 0) unset($json[$key]);
        }
        $json = array_values(array_filter($json));

        return $json;
    }

    public function getModelStats(Directory $directory, $view, $ids)
    {
        $models = array();
        $dirs = array();
        $attempt = $this->em
            ->getRepository('SimpleITClaireExerciseBundle:CreatedExercise\Attempt')
        ;
        $answer = $this->em
            ->getRepository('SimpleITClaireExerciseBundle:CreatedExercise\Answer')
        ;
        #C'est horrible mais ca permet d'avoir l'id et le directoryName
        $dirs[$directory->getId()]['name'] = $directory->getName();
        $dirs[$directory->getId()]['models'] = $this->stats(
        #$dirs[$directory->getId().'+#='.$directory->getName()]= $this->stats(
            $directory,
            $attempt,
            $answer,
            $view,
            $ids
        );
        foreach($directory->getSubs() as $sub){
            $dirs[$sub->getId()]['name'] = $sub->getName();
            $dirs[$sub->getId()]['models'] = $this->stats(
            #$dirs[$sub->getId().'+#='. $sub->getName()] = $this->stats(
                $sub,
                $attempt,
                $answer,
                $view,
                $ids
            );
        }
        return $dirs;
    }
    public function stats(Directory $directory, $attempt, $answer,$view, $ids)
    {
        $models = array();
        foreach($directory->getModels() as $model){
            $models[$model->getId()]['title'] = $model->getTitle();
            $models[$model->getId()]['userAnswer'] = $answer->
            uniqueUsersByModel($model->getId(),$view, $ids)[0]['total']
            ;
            $models[$model->getId()]['userNoAnswer'] =  $attempt->
            uniqueUsersByModel($model->getId(),$view, $ids)[0]['total']
            ;
            $models[$model->getId()]['avgAttempt'] = $attempt->
            averageAttemptByModel($model->getId(),$view,$ids)[0]['avg']
            ;
            $models[$model->getId()]['avgAnswer'] = $answer->
            averageAnswerByModel($model->getId(),$view, $ids)[0]['avg']
            ;
            $models[$model->getId()]['avgMark'] = $answer->
            averageMarkByModel($model->getId(),$view, $ids)[0]['avg']
            ;
            $models[$model->getId()]['directoryId'] =  $directory->getId();
            $models[$model->getId()]['json'] = $this->JSONstats($answer,$directory,$model,$view,$ids);
        }
        return $models;
    }
    public function getIdUsers(Directory $directory, $view)
    {
        $ids = array();
        $users =$directory->realUsers();
        #return  array_column($this->em->getRepository('SimpleITClaireExerciseBundle:AskerUser')
        #->getArrayStudents($directory->getId(),$view->getStartDate(),$view->getEndDate()),'id');
        foreach($users as $user){
            if ($user->isOnlyStudent()){
                if($view){
                    $old = new \DateTime("2999-01-01");
                    foreach($user->getDirectories() as $aud){
                        if ($aud->getDirectory()->getId()  == $directory->getId()){
                            $old = $aud->getEndDate();
                            break;
                        }
                    }
                    $old = new \DateTime("2999-01-01");
                    foreach($user->getLogs() as $log){
                        if ($log->getLoggedAt() >= $view->getStartDate()
                            && $log->getLoggedAt() <= $view->getEndDate()
                            && $old >= $view->getEndDate()
                        ){
                            $ids[] = $user->getId();
                            break;
                        }
                    }
                }else{
                    $ids[] = $user->getId();
                }
            }
        }
        return $ids;


    }
    public function getUsernames(Directory $directory, $view)
    {
        $ids = array();
        $users = $directory->realUsers();
        foreach($users as $user){
            if ($user->isOnlyStudent()){
                if($view){
                    $old = new \DateTime("2999-01-01");
                    foreach($user->getDirectories() as $aud){
                        if ($aud->getDirectory()->getId()  == $directory->getId()){
                            $old = $aud->getEndDate();
                            break;
                        }
                    }
                    $old = new \DateTime("2999-01-01");
                    foreach($user->getLogs() as $log){
                        if ($log->getLoggedAt() >= $view->getStartDate()
                            && $log->getLoggedAt() <= $view->getEndDate()
                            && $old >= $view->getEndDate()
                        ){
                            $ids[$user->getId()] = $user->getUsername();
                            break;
                        }
                    }
                }else{
                    $ids[$user->getId()] = $user->getUsername();
                }
            }
        }
        return $ids;


    }
    public function getUsers(Directory $directory, $view)
    {
        $this->getEntityManager();
        $finalUsers = array();
        $users =$directory->realUsers();
        #return  array_column($this->em->getRepository('SimpleITClaireExerciseBundle:AskerUser')
        #->getArrayStudents($directory->getId(),$view->getStartDate(),$view->getEndDate()),'id');
        foreach($users as $user){
            if ($user->isOnlyStudent()){
                if($view){
                    $old = new \DateTime("2999-01-01");
                    foreach($user->getDirectories() as $aud){
                        if ($aud->getDirectory()->getId()  == $directory->getId()){
                            $old = $aud->getEndDate();
                            break;
                        }
                    }
                    $old = new \DateTime("2999-01-01");
                    foreach($user->getLogs() as $log){
                        if ($log->getLoggedAt() >= $view->getStartDate()
                            && $log->getLoggedAt() <= $view->getEndDate()
                            && $old >= $view->getEndDate()
                        ){
                            $finalUsers[] = $user;
                            break;
                        }
                    }
                }else{
                    $finalUsers[] = $user;
                }
            }
        }
        return $finalUsers;

    }


    public function getColumnStats(Directory $directory, $model, $view, $ids)
    {
        $models = array();
        $dirs = array();
        $answer = $this->em
            ->getRepository('SimpleITClaireExerciseBundle:CreatedExercise\Answer')
        ;
        $dirs[$directory->getName()]= $this->JSONstats(
            $directory,
            $model,
            $answer,
            $view,
            $ids
        );
        foreach($directory->getSubs() as $sub){
            $dirs[$sub->getName()] = $this->JSONstats(
                $sub,
                $model,
                $answer,
                $view,
                $ids
            );
        }
        return $dirs;
    }


    public function hasView(Directory $directory)
    {
        $totalPeda= $this->em
            ->getRepository('SimpleITClaireExerciseBundle:Pedagogic')
            ->periodByCode($directory->getCode())
        ;
        $totalViews = array();
        foreach($directory->getStatViews() as $view){
            $totalViews[] =  $view->getRefPedagogic();
        }
        foreach($totalPeda as $peda){
            if (isset($peda['period'])){
                if (!in_array($peda['year']."-".$peda['period'], $totalViews)){
                    $view = new StatView();
                    $view->setRefPedagogic($peda['year']."-".$peda['period']);
                    $view->setDirectory($directory);
                    if(preg_match("/GP1AUTOM/", $peda['period'])){
                        $view->setName($peda['year']."/". explode('-',$peda['period'])[0]);
                        $view->setStartDate(new \DateTime($peda['year']."-08-15"));
                        $view->setEndDate(new \DateTime(($peda['year']+1)."-01-15"));
                    }else if (preg_match("/GP2PRINT/", $peda['period'])){
                        $view->setName(($peda['year']+1)."/". explode('-',$peda['period'])[0]);
                        $view->setStartDate(new \DateTime(($peda['year']+1)."-01-15"));
                        $view->setEndDate(new \DateTime(($peda['year']+1)."-08-15"));
                    }else{
                        $view->setName($peda['year']."/". explode('-',$peda['period'])[0]);
                        $view->setStartDate(new \DateTime($peda['year']."-08-15"));
                        $view->setEndDate(new \DateTime(($peda['year']+1)."-08-15"));
                    }
                    $this->em->persist($view);
                    try{
                        $this->em->flush();
                        $this->em->refresh($directory);
                    }catch(\Exception $e){
                        die('Une erreur est survenue!');
                    }
                }
            }
        }
    }
    // Statistiques des étudiants dans le tableau
    public function getPreviewStats(Directory $directory, $users, $view)
    {
        $attempt = $this->em
            ->getRepository('SimpleITClaireExerciseBundle:CreatedExercise\Attempt')
        ;

        $stats = array();
        foreach ($users as $key => $user) {
            $stat = $this->directoryRepository->
            getPreviewStats($directory->getId(),$user->getId(),$view)[0]
            ;

            $stats[$key]['user'] = $user;
            $stats[$key]['count1'] = $stat['count1'];
            $stats[$key]['count2'] = $stat['count2'];
            if($stats[$key]['count1'] > 0){
                $stats[$key]['mark'] = round($stat['mark'],2);
                $stats[$key]['firstDate'] = $stat['firstDate'];
                $stats[$key]['lastDate'] = $stat['lastDate'];
                $stats[$key]['days'] = $stat['days'];

                if($stats[$key]['count2'] > 0){
                    $stats[$key]['firstDate2'] = $stat['firstDate2'];
                    $stats[$key]['lastDate2'] = $stat['lastDate2'];
                }
                else{
                    $stats[$key]['firstDate2'] = "-";
                    $stats[$key]['lastDate2'] = "-";
                }
            }
            else{
                $stats[$key]['mark'] = "-";
                $stats[$key]['firstDate'] = "-";
                $stats[$key]['lastDate'] = "-";
                $stats[$key]['firstDate2'] = "-";
                $stats[$key]['lastDate2'] = "-";
                $stats[$key]['days'] = "-";
            }
        }
        return $stats;
    }

    public function exportTomuss($model, $users, $view)
    {
        $answer = $this->em
            ->getRepository('SimpleITClaireExerciseBundle:CreatedExercise\Answer')
        ;
        return $answer->exportTomuss($model, $users, $view);
    }

    public function changeVisibility(AskerUser $user, Directory $directory)
    {
        if (
            $directory->getOwner()->getId() == $user->getId()
            || $user->isAdmin()
            || $directory->hasManager($user)
        ){
            if ($directory->getIsVisible()){
                $directory->setIsVisible(false);
            }else{
                $directory->setIsVisible(true);
            }
            $this->em->flush();
        }else{
            throw new AccessDeniedException();
        }

    }
    // Permet de récupérer les informations de l'étudiant pour afficher un diagramme sunburst
    public function JSONUserModelsStats($directory,$user,$view)
    {
        $dirs = $this->directoryRepository->
        getSubDirsStats($directory->getId(),$user->getId(),$view)
        ;

        foreach ($dirs as $key => $dir) {
            $dirs[$key]['models'] = $this->directoryRepository->
            getModelsStats($dir['id'],$user->getId(),$view)
            ;
        }

        return $dirs;
    }
    // Permet de récupérer les informations de l'étudiant pour afficher une timeline
    public function JSONUserTimeStats($directory,$user,$view)
    {
        $answer = $this->em
            ->getRepository('SimpleITClaireExerciseBundle:CreatedExercise\Answer')
        ;
        $dirs = $this->directoryRepository->
        getSubDirs($directory->getId())
        ;

        $json = array();
        foreach ($dirs as $dir) {
            $json[] = $answer->
            getAllAnswers($dir['id'],$user->getId(),$view)
            ;
        }
        return $json;
    }
}

